mp3diags (1.5.01-2) unstable; urgency=medium

  * Bump debhelper-compat to use (= 13)
  * Mark autopkgtest as superficial (Closes: #969846)
  * debian/control:
    + Add new line at end of file
    + Document Rules-Requires-Root field as 'no'
  * debian/source/lintian-overrides: Update renamed lintian tag
  * Bump Standards-Vesion to 4.5.1. No changes required
  * Fix autopkgtest by adding check-output runtime test

 -- Josue Ortega <josue@debian.org>  Mon, 04 Jan 2021 20:00:40 -0600

mp3diags (1.5.01-1) unstable; urgency=medium

  * New upstream release (1.5.01-1)
    - Refresh debian/patches
    - debian/rules: Update icons name at override_dh_install
    - Update binary name at installation
  * Update debian watch file
  * Update package to use Qt5. Closes: #875057
  * Update package to use debhelper-compat (= 12)
    - debian/rules: Remove unnecessary argument --parallel
  * Update Vcs-* to use salsa.debian.org.
  * Add debian/clean to remove unsafe symlink from source
  * Bump Standards-Version to 4.4.0:
    - debian/copyright: Update Format field to use https
  * Add debian/source/lintian-overrides
  * Add Debian CI tests

 -- Josue Ortega <josue@debian.org>  Fri, 23 Aug 2019 18:44:15 -0600

mp3diags (1.2.03-1) unstable; urgency=medium

  * New upstream release.
  * Removes debian/patches/01-fix-gcc6-compat.patch no longer necessary
    since upstream had applied it.
  * debian/patches: Refresh patches to work with new upstream code.
  * Adds debian/patches/01-fix-format-not-a-string-literal.patch.

 -- Josue Ortega <josue@debian.org>  Wed, 03 Aug 2016 21:48:29 -0600

mp3diags (1.2.02-4) unstable; urgency=medium

  * Adds debian/patches/01/fix-gcc6-compat.patch (Closes: #811666)

 -- Josue Ortega <josue@debian.org>  Sun, 24 Jul 2016 14:21:10 -0600

mp3diags (1.2.02-3) unstable; urgency=medium

  * debian/control:
    - Updates Vcs-* to secure uri.
    - Standards-Version updated to 3.9.8, debian/menu file has been
      removed.
  * debian/rules: Adds DEB_BUILD_MAINT_OPTIONS.

 -- Josue Ortega <josue@debian.org>  Sun, 03 Jul 2016 21:25:16 +0200

mp3diags (1.2.02-2) unstable; urgency=medium

  * Upload to unstable.

 -- Josue Ortega <josueortega@debian.org.gt>  Sat, 09 May 2015 16:04:56 -0600

mp3diags (1.2.02-1) experimental; urgency=medium

  * New upstream version.
  * Standards version updated to 3.9.6, no changes required.
  * debian/copyright: Updated copyright information for src/images/zoom-in.png
  * debian/control: Removed Suggests field, no longer needed.

 -- Josue Ortega <josueortega@debian.org.gt>  Sun, 29 Mar 2015 20:05:59 -0600

mp3diags (1.2.01-1) unstable; urgency=low

  * New upstream version
  * debian/control:
    -Removes mp3diags-doc since upstream does not include
     doc directory in the new version.
    -Adds new dependency: libboost-program-options-dev.
  * debian/patches:
    Patches updated to work with the new version:
     -01-disable_updates.patch
     -03-pass_flags_to_qmake.patch
  * debian/patches/desktop-file-keywords.patch: removed since it is
    include by upstream in the new version.
  * debian/watch: Fix watch file.
  * Standards version updated to 3.9.5, no changes required.
  * debian/control Added VCS-* Fields

 -- Josue Ortega <josueortega@debian.org.gt>  Sat, 01 Feb 2014 18:44:48 -0600

mp3diags (1.0.12.079-2) unstable; urgency=low

  * Fix build failure with boost 1.53:
    -debian/patches/03-pass_flags_to_qmake.patch -lboost_serialization-mt
     changed to -lboost_serialization since it is not needed in the new
     version (Closes: #709575).
  * Upload to unstable
  * debian/patches/desktop-file-keywords added in order to make easier
    the search in enviroments like Gnome or Unity

 -- Josue Ortega <josueortega@debian.org.gt>  Sat, 15 Jun 2013 18:38:18 -0600

mp3diags (1.0.12.079-1) experimental; urgency=low

  * New upstream release:
    - fix Discogs issue with albums that have more than twenty tracks.
  * Fix the failing of the package second build.
  * debian/control Vcs-* field removed.
  * Compat level changed to 9.
  * Fix hardening-no-relro and hardening-no-fortify-functions issues.
  * Standards version updated to 3.9.4, no changes required.

 -- Josue Ortega <josueortega@debian.org.gt>  Mon, 26 March 2013 19:15:07 -0600

mp3diags (1.0.11.076-3) unstable; urgency=low

  * new upload, in order to fix the bug #689811 to unstable and wheezy
    (Closes: #689811)

 -- Josue Ortega <josueortega@debian.org.gt>  Sun, 21 Oct 2012 19:22:59 -0600

mp3diags (1.0.11.076-2) experimental; urgency=low

  * New Maintainer (Closes: #620470)
  * debian/copyright fixes incorrect license entries (Closes: #689811)

 -- Josue Ortega <josueortega@debian.org.gt>  Sat, 13 Oct 2012 23:26:49 -0600

mp3diags (1.0.11.076-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
  * Drop 04-gcc47.patch, applied upstream.

 -- Alessio Treglia <alessio@debian.org>  Sun, 10 Jun 2012 16:26:08 +0200

mp3diags (1.0.10.065-2) unstable; urgency=low

  * QA upload.
  * Update debian/copyright to machine-readable format.
  * Port to GCC 4.7 (Closes: #672053):
    - Add 04-gcc47.patch to fix missing #includes.
    - Compile with -fpermissive.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Wed, 09 May 2012 14:56:03 -0700

mp3diags (1.0.10.065-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
  * Update debian/copyright.

 -- Alessio Treglia <alessio@debian.org>  Tue, 03 Jan 2012 08:54:26 +0100

mp3diags (1.0.09.063-1) unstable; urgency=low

  * QA upload.
  * New upstream release:
    - made Discogs queries work again, after a Discogs API change
    - added close button for windows on Gnome 3

 -- Alessio Treglia <alessio@debian.org>  Mon, 19 Sep 2011 11:13:25 +0200

mp3diags (1.0.08.053-1) unstable; urgency=low

  * QA upload.
  * Imported Upstream version 1.0.08.053:
    - Fix crash when trying to save after error at startup.
  * Enable parallel builds.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 07 Jun 2011 12:23:02 +0200

mp3diags (1.0.07.052-2) unstable; urgency=low

  * QA upload.
  * Orphaning this.
  * Bump Standards-Version.

 -- Alessio Treglia <alessio@debian.org>  Sat, 02 Apr 2011 09:29:47 +0200

mp3diags (1.0.07.052-1) unstable; urgency=low

  * Update watch file.
  * New upstream bugfix-only release.
  * Drop 02-libz.patch, no longer needed.
  * Refresh patches.

 -- Alessio Treglia <alessio@debian.org>  Tue, 04 Jan 2011 10:23:03 +0100

mp3diags (1.0.06.051-1) unstable; urgency=low

  * New upstream release.
  * Add Vcs tags, move packaging to git.
  * Update my email address.
  * Add debian/gbp.conf.
  * Add .gitignore file.
  * Switch to format 3.0 (quilt).
  * Remove DM-Upload-Allowed: yes field.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 06 Jul 2010 23:39:16 +0200

mp3diags (1.0.05.050-2) unstable; urgency=low

  * Rename binary and other associated files to the lowercase form;
    thanks to Carlos Raviola for reporting this (Closes: #578991).
  * Update debian/copyright.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Sat, 24 Apr 2010 09:36:42 +0200

mp3diags (1.0.05.050-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Sat, 20 Mar 2010 11:12:42 +0100

mp3diags (1.0.04.049-1) unstable; urgency=low

  * New upstream bugfix release.
  * Bump Standards.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Mon, 08 Mar 2010 14:14:20 +0100

mp3diags (1.0.03.048-1) unstable; urgency=low

  * New upstream relelase.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Sun, 31 Jan 2010 18:28:11 +0100

mp3diags (1.0.02.047-1) unstable; urgency=low

  * New upstream release.
  * Allow uploads by DM.
  * Adjust versioned build-dependencies to help backporters.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Tue, 12 Jan 2010 23:40:18 +0100

mp3diags (1.0.01.046-1) unstable; urgency=low

  * New upstream release:
    - Fix a segmentation fault with Qt 4.6 (Closes: #560115).
  * Pass even the noopt flag (and the other ones) to gcc;
    really Closes: #560025.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Wed, 09 Dec 2009 16:09:19 +0100

mp3diags (1.0.00.045-2) unstable; urgency=low

  * debian/patches/03-pas_flags_to_qmake.patch:
    - Pass C{,XX}FLAGS to the compiler (Closes: #560025).

 -- Alessio Treglia <quadrispro@ubuntu.com>  Tue, 08 Dec 2009 18:49:54 +0100

mp3diags (1.0.00.045-1) unstable; urgency=low

  * New upstream release:
    - Wording changes to reflect non-beta status.
    - Pressing CTRL+C when viewing full-size images in the tag editor or in
      "Tag details" in the main window copies the image to the clipboard.
    - Added "Rating" and changed field order in "Tag details" to match the Tag
      editor.
  * Refresh patches.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Mon, 30 Nov 2009 14:00:14 +0100

mp3diags (0.99.06.044-2) unstable; urgency=low

  * Add quilt support.
  * debian/patches/01-disable_updates.patch:
    - Don't check for available updates (Closes: #557030).
  * debian/patches/02-libz.patch:
    - Prevent FTBFS with binutils-gold (Closes: #556915).

 -- Alessio Treglia <quadrispro@ubuntu.com>  Wed, 25 Nov 2009 12:12:01 +0100

mp3diags (0.99.06.044-1) unstable; urgency=low

  * New upstream bugfix-release, bugfixes and improvements:
    - fixed a crash in folder filter
    - fixed bug causing non-normalized files having any TXXX frame to appear
      normalized
    - case is ignored for file extension, so .Mp3 or .mP3 files are recognized
    - better support and more consistent handling for TXXX and text frames in
      ID3V2
    - reduced number of locales by eliminating redundant ones
    - disabled CTRL+A selection in the main window
    - static link for serialization
    - added trace details for web downloads

 -- Alessio Treglia <quadrispro@ubuntu.com>  Thu, 05 Nov 2009 10:33:35 +0100

mp3diags (0.99.06.043-1) unstable; urgency=low

  * New upstream release.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Mon, 02 Nov 2009 23:14:47 +0100

mp3diags (0.99.06.042-1) unstable; urgency=low

  * New upstream release.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Sun, 25 Oct 2009 09:58:33 +0100

mp3diags (0.99.06.041-1) unstable; urgency=low

  * Initial release (Closes: #551180).

 -- Alessio Treglia <quadrispro@ubuntu.com>  Sat, 17 Oct 2009 10:50:44 +0200
